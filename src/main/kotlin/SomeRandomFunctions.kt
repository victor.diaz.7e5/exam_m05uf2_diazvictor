/**
 * Generates a list with gradually increasing numbers (n, n+1 ,n+2 ,... ,m ). It's size is determined by 2 values (based on the amount of numbers from the 1st value
 * to the 2nd).
 * @param listStart initial value for the lits's size's determination, it also sets the list's 1st value.
 * @param listEnd final value for the lits's size's determination, it also sets the list's last value.
 * @return a mutableListOf<Int>() which follows the initial description's logic.
 */
//input m=3, n=7 output 3 4 5 6 7
fun autoListBySize(listStart: Int, listEnd: Int): List<Int> {
    val list = mutableListOf<Int>()
    for (i in listStart..listEnd) {
        list.add(i)
    }
    return list
}

/**
 * Returns the minimum value (Int) from a list.
 * @param list List to analyze.
 * @return the minimum value found within the inputted List.
 */
fun minValue(list: List<Int>): Int {
    var currentMin = 0
    for (i in list.indices) {
        if (i < currentMin) {
            currentMin = i
        } else if (i == currentMin) {
            currentMin = i
        }
    }
    return currentMin
}

/**
 * Based on input (which represents a grade), returns a grade-based rating (e.g. excellent).
 * @param grade input based on which the function will determine a grade on.
 * @return the grade's corresponding rating, that one being a String.
 */
// S'esepra el seguent comportament
//  0: No presentat
//  1-4: Insuficient
//  5-6: Suficient
//  7-8: Notable
//  9: Excel·lent
//  10: Excel·lent + MH
fun select(grade: Int): String {
    return when (grade) {
        1, 2, 3, 4 -> "Insuficient"
        0 -> "No presentat"
        10 -> "Excelent + MH"
        5, 6 -> "Suficient"
        7 -> "Notable"
        else -> "No valid"
    }
}

/**
 * Will return a list with all the positions i which a word (String) matches a certain letter (Char).
 * @param word word to be analyzed to match with the letter on all its positions.
 * @param letterToMatch letter or character to match to (target).
 * @return returns the list with all the positions (based on numbers, Int(s)) that had a match.
 */
// De tota la paraula
fun charPositionsInString(word: String, letterToMatch: Char): List<Int> {
    val listOfPositions = mutableListOf<Int>()
    var coincidencies = 0 //useless, or residual, (or i am dumb)
    for (i in 0 until word.lastIndex) {
        if (word[i] == letterToMatch) {
            listOfPositions.add(i)
        }
    }
    return listOfPositions
}

/**
 * Shows in which position a certain letter (or Char) is first found within a word (or String).
 * @param word word to be analyzed to match with the letter until 1 match happens (otherwise until exhausted).
 * @param letterMatchFirst letter or character to match to (target).
 * @return returns the first position where the Char was found within the string. In case of there being no match at all, returns -1 instead.
 */
//abc="marieta" b='a' -> 1
//abc="marieta" b='b' -> -1
fun firstOccurrencePosition(word: String, letterMatchFirst: Char): Int {
    val pos: MutableList<Int> = mutableListOf() //useless, or residual, (or i am dumb)
    for (i in 0 until word.length) {
        if (word[i] == letterMatchFirst) {
            return i
        }
    }
    return -1
}

/**
 * Will return the rounded up form of a grade (Double). If the grade's value was below 5.0, it will be rounded down instead.
 * @param grade grade to round up (or down).
 * @return returns the rounded-up grade, or rounded down if the grade's value was below 5.0.
 */
/*
La funció ha de retornar la nota arredonida. 8.7 -> 9, 7.5 -> 8
Excepció, Si la nota no arriba a un 5, no obtindrà el 5. És a dir: 4.9 -> 4
 */
fun gradeRoundUp(grade: Double): Int {
    return grade.toInt()
}