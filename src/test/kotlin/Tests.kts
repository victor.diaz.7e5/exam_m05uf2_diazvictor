package cat.itb.victordiaz7e5.dam.m03

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Tests{
    @Test
    fun autoListBySizeTest() {
        val listStart = 4
        val listEnd = 6
        val expected = listOf(4,5,6)
        assertEquals(expected, autoListBySize(listStart, listEnd))
    }

    @Test
    fun minValueTest() {
        val list = listOf(4,5,6)
        val expected = 4
        assertEquals(expected, minValue(list))
        //failed because the program sets its default minimum at 0.
    }

    @Test
    fun gradeConversorTest() {
        val grade = 6
        val expected = "Suficient"
        assertEquals(expected, gradeConversor(grade))
    }

}

//Note: I utilized another project to conduct the tests, due to my uncapability to set up Junit5 in this project.